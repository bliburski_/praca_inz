<?php

namespace App\EventListener;

use App\Exception\ExceptionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        if ($exception instanceof ExceptionInterface) {
            $event->setResponse($exception->getExceptionResponse());
        } else {
            $response = new JsonResponse();
            if ($exception instanceof HttpExceptionInterface) {
                $response->setStatusCode($exception->getStatusCode());
                $response->headers->replace($exception->getHeaders());
            } else {
                $response->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            }
            $message = new \stdClass();
            $message->statusCode = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
            $message->message = $exception->getMessage();
            $response->setContent(json_encode($message));
            $event->setResponse($response);
        }
    }
}
