# GET products/
**Example call** 

`http://localhost:5000/products`

**Example response** (object)

```json
[
  {
    "_index": "products",
    "_type": "_doc",
    "_id": "1",
    "_score": 1,
    "_source": {
      "name": "Saab 93 1.9 TID 2009",
      "suggest_name": {
        "input": [
          "Saab 93 1.9 TID 2009",
          "sweed"
        ]
      },
      "attributes": [
        {
          "name": "manufacturer",
          "value": "Saab"
        },
        {
          "name": "model",
          "value": "93"
        },
        {
          "name": "generation",
          "value": "2008-2011"
        },
        {
          "name": "weight",
          "value": "1.6",
          "unit": "t"
        }
      ],
      "location": [
        19.724685,
        51.0464293
      ]
    }
  },
  {
    "_index": "products",
    "_type": "_doc",
    "_id": "2",
    "_score": 1,
    "_source": {
      "name": "Audi A4 new model!",
      "suggest_name": {
        "input": [
          "Audi A4 new model!",
          "new"
        ]
      },
      "attributes": [
        {
          "name": "manufacturer",
          "value": "Audi"
        },
        {
          "name": "model",
          "value": "A4"
        },
        {
          "name": "generation",
          "value": "2017-2020 TDI"
        },
        {
          "name": "color",
          "value": "Red"
        }
      ],
      "location": [
        19.724631,
        51.0464254
      ]
    }
  }
]
```

# GET products/{productId}
**URL Params** 

productId[int] - product id

**Example call** 

`http://localhost:5000/products/2`

**Example response** (object)

```json
{
  "_index": "products",
  "_type": "_doc",
  "_id": "2",
  "_version": 2,
  "_seq_no": 4,
  "_primary_term": 1,
  "found": true,
  "_source": {
    "name": "Audi A4 new model!",
    "suggest_name": {
      "input": [
        "Audi A4 new model!",
        "new"
      ]
    },
    "attributes": [
      {
        "name": "manufacturer",
        "value": "Audi"
      },
      {
        "name": "model",
        "value": "A4"
      },
      {
        "name": "generation",
        "value": "2017-2020 TDI"
      },
      {
        "name": "color",
        "value": "Red"
      }
    ],
    "location": [
      19.724631,
      51.0464254
    ]
  }
}
```