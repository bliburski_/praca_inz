# GET suggest/{indexName}/{term}
**URL Params** 

indexName[string] - name of index with suggester field (min 2 chars)

term[string] - term to look for

**Example call** 

`http://localhost:5000/suggest/products/au`

**Example response** (object)

```json
{
  "default": [
    {
      "text": "au",
      "offset": 0,
      "length": 2,
      "options": [
        {
          "text": "Audi A4 new model!",
          "_index": "products",
          "_type": "_doc",
          "_id": "2",
          "_score": 1,
          "_source": {
            "name": "Audi A4 new model!",
            "suggest_name": {
              "input": [
                "Audi A4 new model!",
                "new"
              ]
            },
            "attributes": [
              {
                "name": "manufacturer",
                "value": "Audi"
              },
              {
                "name": "model",
                "value": "A4"
              },
              {
                "name": "generation",
                "value": "2017-2020 TDI"
              },
              {
                "name": "color",
                "value": "Red"
              }
            ]
          }
        }
      ]
    }
  ]
}
```

