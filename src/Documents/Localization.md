# PUT localization/{productId}
**URL Params** 

productId[int] - product id

**Request body params**

lat[float] - latitude
long[float] - longitude

**Example call** 

`http://localhost:5000/localization/2`

body:

```json
{
	"lat": 19.724631,
	"lon": 51.0464254
}
```
**Example response** (object)

```json
[
    true,
    "updated index: products"
]
```

# GET localization/{productId}
**URL Params** 

productId[int] - product id

**Example call** 

`http://localhost:5000/localization/2`

**Example response** (object)

```json
{
  "_index": "products",
  "_type": "_doc",
  "_id": "2",
  "_version": 3,
  "_seq_no": 3,
  "_primary_term": 1,
  "found": true,
  "_source": {
    "location": [
      19.724685,
      51.0464293
    ]
  }
}
```