<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class ItemAtrributesValues
{
    private $id;

    private $value;

    private $created_time;

    private $update_time;

    private $isActive;

    private $attributes_values_for_items;

    private $attribute_id;

    private $unit_id;

    public function __construct()
    {
        $this->attributes_values_for_items = new ArrayCollection();
        $this->setCreatedTime(new \DateTime());
        $this->setIsActive(true);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getCreatedTime(): ?\DateTimeInterface
    {
        return $this->created_time;
    }

    public function setCreatedTime(\DateTimeInterface $created_time): self
    {
        $this->created_time = $created_time;

        return $this;
    }

    public function getUpdateTime(): ?\DateTimeInterface
    {
        return $this->update_time;
    }

    public function setUpdateTime(?\DateTimeInterface $update_time): self
    {
        $this->update_time = $update_time;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|ItemAtrributesForItemsEntities[]
     */
    public function getAttributesValuesForItems(): Collection
    {
        return $this->attributes_values_for_items;
    }

    public function addAttributesValuesForItem(ItemAtrributesForItemsEntities $attributesValuesForItem): self
    {
        if (!$this->attributes_values_for_items->contains($attributesValuesForItem)) {
            $this->attributes_values_for_items[] = $attributesValuesForItem;
            $attributesValuesForItem->setAttributeValueId($this);
        }

        return $this;
    }

    public function removeAttributesValuesForItem(ItemAtrributesForItemsEntities $attributesValuesForItem): self
    {
        if ($this->attributes_values_for_items->contains($attributesValuesForItem)) {
            $this->attributes_values_for_items->removeElement($attributesValuesForItem);
            // set the owning side to null (unless already changed)
            if ($attributesValuesForItem->getAttributeValueId() === $this) {
                $attributesValuesForItem->setAttributeValueId(null);
            }
        }

        return $this;
    }

    public function getAttributeId(): ?ItemAtrributes
    {
        return $this->attribute_id;
    }

    public function setAttributeId(?ItemAtrributes $attribute_id): self
    {
        $this->attribute_id = $attribute_id;

        return $this;
    }

    public function getUnitId(): ?ItemAtrributesUnits
    {
        return $this->unit_id;
    }

    public function setUnitId(?ItemAtrributesUnits $unit_id): self
    {
        $this->unit_id = $unit_id;

        return $this;
    }
}
