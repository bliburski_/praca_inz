<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class ItemAtrributesUnitsTypes
{
    private $id;

    private $name;

    private $created_time;

    private $update_time;

    private $isActive;

    private $attribute_value_unit;

    public function __construct()
    {
        $this->attribute_value_unit = new ArrayCollection();
        $this->setCreatedTime(new \DateTime());
        $this->setIsActive(true);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedTime(): ?\DateTimeInterface
    {
        return $this->created_time;
    }

    public function setCreatedTime(\DateTimeInterface $created_time): self
    {
        $this->created_time = $created_time;

        return $this;
    }

    public function getUpdateTime(): ?\DateTimeInterface
    {
        return $this->update_time;
    }

    public function setUpdateTime(?\DateTimeInterface $update_time): self
    {
        $this->update_time = $update_time;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|ItemAtrributesUnits[]
     */
    public function getAttributeValueUnit(): Collection
    {
        return $this->attribute_value_unit;
    }

    public function addAttributeValueUnit(ItemAtrributesUnits $attributeValueUnit): self
    {
        if (!$this->attribute_value_unit->contains($attributeValueUnit)) {
            $this->attribute_value_unit[] = $attributeValueUnit;
            $attributeValueUnit->setUnitTypeId($this);
        }

        return $this;
    }

    public function removeAttributeValueUnit(ItemAtrributesUnits $attributeValueUnit): self
    {
        if ($this->attribute_value_unit->contains($attributeValueUnit)) {
            $this->attribute_value_unit->removeElement($attributeValueUnit);
            // set the owning side to null (unless already changed)
            if ($attributeValueUnit->getUnitTypeId() === $this) {
                $attributeValueUnit->setUnitTypeId(null);
            }
        }

        return $this;
    }
}
