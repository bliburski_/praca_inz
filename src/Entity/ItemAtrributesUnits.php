<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class ItemAtrributesUnits
{
    private $id;

    private $name;

    private $created_time;

    private $update_time;

    private $isActive;

    private $attributes_values;

    private $unit_type_id;

    public function __construct()
    {
        $this->attributes_values = new ArrayCollection();
        $this->setCreatedTime(new \DateTime());
        $this->setIsActive(true);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedTime(): ?\DateTimeInterface
    {
        return $this->created_time;
    }

    public function setCreatedTime(\DateTimeInterface $created_time): self
    {
        $this->created_time = $created_time;

        return $this;
    }

    public function getUpdateTime(): ?\DateTimeInterface
    {
        return $this->update_time;
    }

    public function setUpdateTime(?\DateTimeInterface $update_time): self
    {
        $this->update_time = $update_time;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|ItemAtrributes[]
     */
    public function getAttributesValues(): Collection
    {
        return $this->attributes_values;
    }

    public function addAttributesValue(ItemAtrributes $attributesValue): self
    {
        if (!$this->attributes_values->contains($attributesValue)) {
            $this->attributes_values[] = $attributesValue;
            $attributesValue->setUnitId($this);
        }

        return $this;
    }

    public function removeAttributesValue(ItemAtrributes $attributesValue): self
    {
        if ($this->attributes_values->contains($attributesValue)) {
            $this->attributes_values->removeElement($attributesValue);
            // set the owning side to null (unless already changed)
            if ($attributesValue->getUnitId() === $this) {
                $attributesValue->setUnitId(null);
            }
        }

        return $this;
    }

    public function getUnitTypeId(): ?ItemAtrributesUnitsTypes
    {
        return $this->unit_type_id;
    }

    public function setUnitTypeId(?ItemAtrributesUnitsTypes $unit_type_id): self
    {
        $this->unit_type_id = $unit_type_id;

        return $this;
    }
}
