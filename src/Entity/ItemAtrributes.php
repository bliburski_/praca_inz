<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class ItemAtrributes
{
    private $id;

    private $name;

    private $created_time;

    private $update_time;

    private $isActive;

    private $attribute_values;

    public function __construct()
    {
        $this->attribute_values = new ArrayCollection();
        $this->setCreatedTime(new \DateTime());
        $this->setIsActive(true);

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedTime(): ?\DateTimeInterface
    {
        return $this->created_time;
    }

    public function setCreatedTime(\DateTimeInterface $created_time): self
    {
        $this->created_time = $created_time;

        return $this;
    }

    public function getUpdateTime(): ?\DateTimeInterface
    {
        return $this->update_time;
    }

    public function setUpdateTime(?\DateTimeInterface $update_time): self
    {
        $this->update_time = $update_time;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|ItemAtrributesValues[]
     */
    public function getAttributeValues(): Collection
    {
        return $this->attribute_values;
    }

    public function addAttributeValue(ItemAtrributesValues $attributeValue): self
    {
        if (!$this->attribute_values->contains($attributeValue)) {
            $this->attribute_values[] = $attributeValue;
            $attributeValue->setAttributeId($this);
        }

        return $this;
    }

    public function removeAttributeValue(ItemAtrributesValues $attributeValue): self
    {
        if ($this->attribute_values->contains($attributeValue)) {
            $this->attribute_values->removeElement($attributeValue);
            // set the owning side to null (unless already changed)
            if ($attributeValue->getAttributeId() === $this) {
                $attributeValue->setAttributeId(null);
            }
        }

        return $this;
    }
}
