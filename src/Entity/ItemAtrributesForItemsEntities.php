<?php

namespace App\Entity;

class ItemAtrributesForItemsEntities
{
    private $id;

    private $attribute_value_id;

    private $item_id;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAttributeValueId(): ?ItemAtrributesValues
    {
        return $this->attribute_value_id;
    }

    public function setAttributeValueId(?ItemAtrributesValues $attribute_value_id): self
    {
        $this->attribute_value_id = $attribute_value_id;

        return $this;
    }

    public function getItemId(): ?ProductsEntities
    {
        return $this->item_id;
    }

    public function setItemId(?ProductsEntities $item_id): self
    {
        $this->item_id = $item_id;

        return $this;
    }
}
