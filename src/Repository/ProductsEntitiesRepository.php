<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

use App\Entity\ProductsEntities;

class ProductsEntitiesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductsEntities::class);
    }

    /**
     * @return ProductsEntities[]
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findAllWithAttributes(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT p.id AS product_id, p.name AS product_name, p.alternative_search_name, ia_val.name AS attr_name, ia.value AS attr_value, units.name AS unit
        FROM `item_attributes_for_items_entities` AS attr_val
        LEFT JOIN `products_entities` as p
            ON p.id = attr_val.item_id
        LEFT JOIN `item_attributes_values` as ia
            ON ia.id = attr_val.attribute_value_id
        LEFT JOIN `item_attributes` as ia_val
            ON ia_val.id = ia.attribute_id
        LEFT JOIN `item_attributes_units` as units
            ON units.id = ia.unit_id
        ';
        $stmt = $conn->prepare($sql);

        $stmt->execute();
        return $stmt->fetchAll();
    }
}