<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

use App\ElasticSearch\ElasticSearchClient;
use App\ElasticSearch\ProductIndexer;
use App\Exception\JsonException;

class ElasticSearchReindexCommand extends Command
{
    protected static $defaultName = 'es:index:products';
    private const PRODUCTS_INDEX = 'products';
    private $indexer;

    public function __construct(ProductIndexer $indexer)
    {
        $this->indexer = $indexer;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Index es products data');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param ProductIndexer $indexer
     * @return null
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Indexing products',
            'Chosen index name: ' . self::PRODUCTS_INDEX,
            'It may take some time...',
            ''
        ]);

        $query = $this->indexer->fetchAndConvertDataFromMySql();
        $esConnection = new ElasticSearchClient(self::PRODUCTS_INDEX, 'bulk', 'POST');
        try {
            $res = $esConnection->sendRequestWithCustomString($query);
        } catch (JsonException $e) {
            throw new JsonException(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, "ES connection problem: $e");
        }

        $output->writeln('Indexing succeeded');

        return null;
    }
}