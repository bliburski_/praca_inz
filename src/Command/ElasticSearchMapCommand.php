<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

use App\ElasticSearch\ElasticSearchClient;


class ElasticSearchMapCommand extends Command
{
    protected static $defaultName = 'es:map';

    protected function configure()
    {
        $this->setDescription('Map es indexes with data in config');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return null
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Mapping data will purge mapped indices, do you want to continue? (y/n)', false);

        if (!$helper->ask($input, $output, $question)) {
            return;
        }

        $finder = new Finder();
        $finder->files()->in(dirname(__DIR__) . '/Resources/config/elasticSearch');

        if ($finder->hasResults()) {
            foreach ($finder as $file) {
                $fileName = $file->getFilenameWithoutExtension();
                $contents = $file->getContents();
                $output->writeln("Found mapping for index: $fileName. Mapping in progress.");
                $esClient = new ElasticSearchClient($fileName, null);
                $res = $esClient->makeMapping($contents);
            }
        }

        $output->writeln("Done.");
        return null;
    }
}