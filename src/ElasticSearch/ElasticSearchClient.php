<?php

namespace App\ElasticSearch;

use stdClass;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpClient\HttpClient;

use App\Exception\JsonException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ElasticSearchClient
{
    private const MODULE_NAME = 'ElasticSearch Client';
    private const SUGGESTIONS_DEFAULT_NAME = 'default';

    public const PARAM_SORT = 'sort';
    public const PARAM_QUERY = 'q';
    public const PARAM_FROM = 'from';
    public const PARAM_SIZE = 'size';
    public const PARAM_INCLUDE = '_source_includes';
    public const PARAM_EXCLUDE = '_source_excludes';
    public const PARAM_ALLOW_PARTIAL_SEARCH = 'allow_partial_search_results';

    private const AVAILABLE_CONDITIONS = [
        'must' => [],
        'must_not' => [],
        'should' => []
    ];
    private $requestMethod;
    private $requestUrl;
    private $query;
    private $urlParams = [];
    private $queriedDocumentId;

    /**
     * ElasticSearchClient constructor.
     * @param string $indexName
     * @param string $requestMethod
     * @param string|null $queryEndpoint
     */
    public function __construct(string $indexName, ?string $queryEndpoint = 'search', string $requestMethod = 'GET')
    {
        $this->setRequestUrl($indexName, $queryEndpoint);
        $this->requestMethod = $requestMethod;
        $this->query = [];
    }

    /**
     * @param int $id
     */
    public function setQueriedDocumentId(int $id)
    {
        $this->queriedDocumentId = $id;
    }

    /**
     * @param string $parameterName
     * @param mixed $parameterValue
     */
    public function addQueryUrlParam(string $parameterName, $parameterValue)
    {
        $this->urlParams[$parameterName] = $parameterValue;
    }

    /**
     * @param string $indexName
     * @param string|null $queryEndpoint
     */
    private function setRequestUrl(string $indexName, ?string $queryEndpoint)
    {
        if (empty($_ENV['ES_PROTOCOL']) || empty($_ENV['ES_CONTAINER']) || empty($_ENV['ES_PORT'])) {
            throw new JsonException(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, self::MODULE_NAME . " error: no ElasticSearch address data");
        }

        $endpoint = !is_null($queryEndpoint) ? "/_{$queryEndpoint}" : '';
        $this->requestUrl = "{$_ENV['ES_PROTOCOL']}://{$_ENV['ES_CONTAINER']}:{$_ENV['ES_PORT']}/{$indexName}{$endpoint}";
    }

    /**
     * @param string $field
     * @param $term
     */
    public function addMustHaveFilter(string $field, $term)
    {
        if (!isset($this->query['must'])) {
            $this->query['must'] = [];
        }
        array_push($this->query['must'], [$this->queryTermIsArray($term), $field, $term]);
    }

    /**
     * @param string $field
     * @param $term
     */
    public function addShouldHaveFilter(string $field, $term)
    {
        if (!isset($this->query['should'])) {
            $this->query['should'] = [];
        }
        array_push($this->query['should'], [$this->queryTermIsArray($term), $field, $term]);
    }

    /**
     * @param string $field
     * @param $term
     */
    public function addMustNotHaveFilter(string $field, $term)
    {
        if (!isset($this->query['must_not'])) {
            $this->query['must_not'] = [];
        }
        array_push($this->query['must_not'], [$this->queryTermIsArray($term), $field, $term]);
    }

    /**
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function queryElasticSearch()
    {
        $esQuery = [];
        if (!empty($this->getQuery())) {
            $esQuery = ['json' => $this->prepareSearchQuery()];
        }

        return $this->makeRequestToElasticSearch($esQuery);
    }

    /**
     * @param string $mappingJson
     * @return array|null
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function makeMapping(string $mappingJson): ?array
    {
        $ifIndexExist = $this->makeRequestToElasticSearch([], 'HEAD');
        if (!is_null($ifIndexExist)) {
            $this->makeRequestToElasticSearch([], 'DELETE');
        }
        return $this->makeRequestToElasticSearch(['json' => json_decode($mappingJson)], 'PUT');
    }

    /**
     * @param string $queryString
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function sendRequestWithCustomString(string $queryString): array
    {
        return $this->makeRequestToElasticSearch([
            'body' => $queryString,
            'headers' =>
                [
                    'Content-Type' => 'application/json',
                ]
        ]);
    }

    /**
     * @param string $jsonString
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function sendRequestWithCustomQueryArray(string $jsonString): array
    {
        return $this->makeRequestToElasticSearch([
            'json' => json_decode($jsonString),
            'headers' =>
                [
                    'Content-Type' => 'application/json',
                ]
        ]);
    }

    /**
     * @param string $term
     * @param string $suggestFieldName
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function makeSuggestionByTerm(string $term, string $suggestFieldName = 'suggest_name'): array
    {
        $suggestQuery['suggest'][self::SUGGESTIONS_DEFAULT_NAME] = [
            'prefix' => $term,
            'completion' => [
                'field' => $suggestFieldName
            ]
        ];

        return $this->makeRequestToElasticSearch(['json' => $suggestQuery]);
    }

    /**
     * @return HttpClientInterface
     */
    private function setupConnection(): HttpClientInterface
    {
        $connectionOptions = [];
        if (!empty($_ENV['ES_USER']) && !empty($_ENV['ES_PASSWORD'])) {
            $connectionOptions = ['auth_basic' => [$_ENV['ES_USER'], $_ENV['ES_PASSWORD']]];
        }

        if (!is_null($this->queriedDocumentId)) {
            $this->requestUrl .= "/$this->queriedDocumentId";
        }

        if (!empty($this->urlParams)) {
            $queryString = '?';
            foreach ($this->urlParams as $key => $param) {
                $queryString .= $key . '=' . $param;
                if ($key !== array_key_last($this->urlParams)) {
                    $queryString .= '&';
                }
            }
            $this->requestUrl .= $queryString;
        }

        return HttpClient::create($connectionOptions);
    }

    /**
     * @param $term
     * @return string
     */
    private function queryTermIsArray($term): string
    {
        if (is_array($term)) {
            return 'terms';
        }
        return 'term';
    }

    /**
     * @return array
     */
    private function getQuery(): array
    {
        return $this->query;
    }

    /**
     * @return object
     */
    private function prepareSearchQuery(): stdClass
    {
        $boolConditions = self::AVAILABLE_CONDITIONS;
        foreach ($boolConditions as $filterName => $value) {
            if (isset($this->getQuery()[$filterName])) {
                foreach ($this->getQuery()[$filterName] as $filter) {
                    $conditions = [$filter[0] => [
                        $filter[1] => $filter[2]
                    ]];
                    array_push($boolConditions[$filterName], $conditions);
                }
            }
        }

        $searchQuery = [
            'query' => [
                'bool' => [
                    'filter' => [
                        'bool' => $boolConditions
                    ]
                ]
            ]
        ];

        return (object)$searchQuery;
    }

    /**
     * @param array $response
     * @throws JsonException
     * @return array
     */
    private function convertHitsFromElasticSearch(array $response) :array
    {
        if (isset($response['hits']) && $response['hits']['total']['value'] > 0) {
            return $response['hits']['hits'];
        }

        if (isset($response['suggest'])) {
            return $response['suggest'];
        }

        if (isset($response['errors']) && $response['errors'] === true) {
            if ($response['items']) {
                throw new JsonException(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, self::MODULE_NAME . " errors: " . $response['items']);
            }
            throw new JsonException(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, self::MODULE_NAME . " unknown error");
        }

        if (isset($response['result']) && $response['result'] === 'updated') {
            return [true, 'updated index: ' . $response['_index']];
        }

        return $response;
    }

    /**
     * @param array $esQuery ['json' => $json] || ['body' => $string]
     * @param string|null $method
     * @return mixed returned array has in first element bool value {false if errors, true if query succeed}, second element of array contains content/error info if exist
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws DecodingExceptionInterface
     *
     * ex. return:
     * [false, 404]
     * [true, [items]]
     */
    private function makeRequestToElasticSearch(array $esQuery, ?string $method = null)
    {
        if (is_null($method)) $method = $this->requestMethod;
        $client = $this->setupConnection();

        try {
            $responses = $client->request($method, $this->requestUrl, $esQuery);
        } catch (TransportExceptionInterface $e) {
            throw new JsonException(JsonResponse::HTTP_NOT_FOUND, self::MODULE_NAME . " error: while executing request $e");
        }

        foreach ($client->stream($responses) as $response => $chunk) {
            if ($chunk->isFirst()) {
                $statusCode = $response->getStatusCode();
                if ($statusCode === 404) {
                    $response->cancel();
                    throw new JsonException(JsonResponse::HTTP_NOT_FOUND, self::MODULE_NAME . " error: did find url you looking for in elastic search API");
                }
                if ($statusCode !== 200) {
                    $response->cancel();
                    throw new JsonException(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, self::MODULE_NAME . " error: http request failed with code: {$statusCode}");
                }
            }

            if ($chunk->isLast()) {
                try {
                    $wholeResponse = $response->getContent();
                } catch (ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface $e) {
                    throw new JsonException(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, self::MODULE_NAME . " error while getting response content: $e");
                }

                if (empty($wholeResponse)) {
                    return true;
                }

                return $this->convertHitsFromElasticSearch($response->toArray());
            }
        }
    }
}