<?php

namespace App\ElasticSearch;

use Doctrine\Common\Persistence\ManagerRegistry;

use App\Entity\ProductsEntities;

class ProductIndexer
{
    private $em;

    /**
     * ProductIndexer constructor.
     * @param ManagerRegistry $em
     */
    public function __construct(ManagerRegistry $em)
    {
        $this->em = $em;
    }

    /**
     * @return string
     */
    public function fetchAndConvertDataFromMySql(): string
    {
        $products = $this->em
            ->getRepository(ProductsEntities::class)
            ->findAllWithAttributes();
        $productsWithMergedAttributes = [];
        $lastProductInLoop = null;
        $tempAttributesArray = [];
        array_push($products, null);
        foreach ($products as $product) {
            if (is_null($lastProductInLoop) || $product['product_id'] === $lastProductInLoop['product_id']) { // same product as in prev loop
                $attributeObject = self::newAttributeToProduct($product);
                array_push($tempAttributesArray, $attributeObject);
            } else {
                $content = self::newProductToIndex($lastProductInLoop, $tempAttributesArray);
                array_push($productsWithMergedAttributes, json_encode($content[0]) . "\n");
                array_push($productsWithMergedAttributes, json_encode($content[1]) . "\n");

                //save new product attribute object
                $tempAttributesArray = [];
                $attributeObject = self::newAttributeToProduct($product);
                array_push($tempAttributesArray, $attributeObject);
            }
            $lastProductInLoop = $product;
        }

        return implode('', $productsWithMergedAttributes);
    }

    /**
     * @param array|null $product
     * @return \stdClass
     */
    static private function newAttributeToProduct(?array $product): \stdClass
    {
        $attributeObject = new \stdClass();
        $attributeObject->name = $product['attr_name'];
        $attributeObject->value = $product['attr_value'];
        if (!is_null($product['unit'])) $attributeObject->unit = $product['unit'];

        return $attributeObject;
    }

    /**
     * @param array $product
     * @param array $tempAttributesArray
     * @return array
     */
    static private function newProductToIndex(array $product, array $tempAttributesArray): array
    {
        $index = [];
        $index['index'] = [
            '_id' => $product['product_id']
        ];

        $suggestPhrase = empty($product['alternative_search_name']) ? [$product['product_name']] : [$product['product_name'], $product['alternative_search_name']];

        $content = [
            'name' => $product['product_name'],
            'suggest_name' => [
                'input' => $suggestPhrase
            ],
            'attributes' => $tempAttributesArray
        ];

        return [$index, $content];
    }
}