<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\ElasticSearch\ElasticSearchClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ProductsController extends AbstractController
{
    /**
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getProducts(): JsonResponse
    {
        $esConnection = new ElasticSearchClient('products');

        return new JsonResponse($esConnection->queryElasticSearch());
    }

    public function getProductById(int $productId): JsonResponse
    {
        $esConnection = new ElasticSearchClient('products', 'doc');
        $esConnection->setQueriedDocumentId($productId);

        return new JsonResponse($esConnection->queryElasticSearch());
    }
}
