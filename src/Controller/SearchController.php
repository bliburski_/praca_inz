<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Exception\JsonException;
use App\ElasticSearch\ElasticSearchClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class SearchController extends AbstractController
{
    /**
     * @param string $indexName
     * @param string $term
     * @return JsonResponse
     * @throws JsonException
     */
    public function suggestByIndexAndTerm(string $indexName, string $term): JsonResponse
    {
        if (strlen($term) < 2) {
            throw new JsonException(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, 'too short ingredient query param');
        }

        $esConnection = new ElasticSearchClient($indexName);
        try {
            $res = $esConnection->makeSuggestionByTerm($term);
        } catch (ClientExceptionInterface | DecodingExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface | TransportExceptionInterface $e) {
            throw new JsonException(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, "Error with suggester $e");
        }

        return new JsonResponse($res);
    }

}
