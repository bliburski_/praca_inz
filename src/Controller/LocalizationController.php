<?php

namespace App\Controller;

use App\ElasticSearch\ProductIndexer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Component\HttpFoundation\Request;

use App\Exception\JsonException;
use App\ElasticSearch\ElasticSearchClient;

class LocalizationController extends AbstractController
{
    private const PRODUCTS_INDEX = 'products';

    /**
     * @param int $productId
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getProductLocationById(int $productId, ProductIndexer $indexer): JsonResponse
    {
        $esConnection = new ElasticSearchClient('products', 'doc');
        $esConnection->setQueriedDocumentId($productId);
        $esConnection->addQueryUrlParam($esConnection::PARAM_INCLUDE, 'location');

        return new JsonResponse($esConnection->queryElasticSearch());
    }

    /**
     * @param int $productId
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function setProductLocation(int $productId)
    {
        $request = Request::createFromGlobals();
        $requestBody = json_decode($request->getContent());

        if (!isset($requestBody->lat)) {
            throw new JsonException(JsonResponse::HTTP_BAD_REQUEST, 'No latitude in request body');
        }
        if (!isset($requestBody->lon)) {
            throw new JsonException(JsonResponse::HTTP_BAD_REQUEST, 'No longitude in request body');
        }

        $esConnection = new ElasticSearchClient(self::PRODUCTS_INDEX, 'update', 'POST');
        $esConnection->setQueriedDocumentId($productId);
        $localization = [
            'doc' => [
                'location' => [$requestBody->lat, $requestBody->lon]
            ]
        ];

        $res = $esConnection->sendRequestWithCustomQueryArray(json_encode($localization));

        return new JsonResponse($res);
    }

}
