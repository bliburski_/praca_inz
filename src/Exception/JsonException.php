<?php

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\JsonResponse;

class JsonException extends HttpException implements ExceptionInterface
{
    /**
     * JsonException constructor.
     * @param $statusCode
     * @param $message
     */
    public function __construct(int $statusCode, string $message)
    {
        parent::__construct($statusCode, $message, null, ['Content-Type' => 'application/problem+json'], 0);
    }

    /**
     * @return JsonResponse
     */
    public function getExceptionResponse()
    {
        $response = new JsonResponse();
        $response->setStatusCode(self::getStatusCode());
        $response->headers->replace(self::getHeaders());

        $message = (object)[];
        $message->statusCode = self::getStatusCode();
        $message->message = self::getMessage();

        $response->setContent(
            json_encode($message)
        );

        return $response;
    }
}
