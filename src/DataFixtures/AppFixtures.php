<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\ProductsEntities;
use App\Entity\ItemAtrributes;

class AppFixtures extends Fixture
{
    private const TESTING_ATTRIBUTES = [
        'weight', 'color', 'manufacturer', 'model', 'generation'
    ];

    /**
     * @param ObjectManager $manager
     */
    private function buildProductFixture(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            $product = new ProductsEntities();
            $product->setName('Product__' . $i);
            $product->setPrice(mt_rand(10, 100));
            $manager->persist($product);
        }
    }

    /**
     * @param ObjectManager $manager
     */
    private function buildAttributesFixture(ObjectManager $manager)
    {
        foreach (self::TESTING_ATTRIBUTES as $attr) {
            $newAttr = new ItemAtrributes();
            $newAttr->setName($attr);
            $manager->persist($newAttr);
        }
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->buildProductFixture($manager);
        $this->buildAttributesFixture($manager);
        $manager->flush();
    }
}
