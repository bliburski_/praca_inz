<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\ItemAtrributesUnitsTypes;
use App\Entity\ItemAtrributesUnits;

class DictionariesFixtures extends Fixture
{
    private const UNITS_TYPES = [
        'weight' => [
            'g',
            'kg',
            't',
        ],
        'metric_sizes' => [
            'mm',
            'm',
            'km'
        ]
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (self::UNITS_TYPES as $unitType => $units) {
            $newUnitType = new ItemAtrributesUnitsTypes();
            $newUnitType->setName($unitType);
            foreach ($units as $unit) {
                $newUnit = new ItemAtrributesUnits();
                $newUnit->setName($unit);
                $newUnit->setUnitTypeId($newUnitType);
                $manager->persist($newUnit);
            }
            $manager->persist($newUnitType);
        }

        $manager->flush();
    }
}
