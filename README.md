# Project init
`docker-compose build`

`docker-compose up`

**Go to container**
`docker exec -it -u dev praca_php bash` -> go to symfony container console

**If elasticsearch container is exiting with code 78** then change take look here:

https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html

**Data fixtures load**

`php bin/console doctrine:fixtures:load`